# Tutorial: Internationalising your plugin

This is the source code for the 'Internationalising Your Plugin' Jira Plugin Tutorial, 
which is available at: [Tutorial - Internationalising your plugin][1].

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

* atlas-run   -- installs this plugin into Jira and starts it on http://localhost:2990/jira
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running Jira instance
* atlas-help  -- prints description for all commands in the SDK

 [1]: https://developer.atlassian.com/jiradev/jira-platform/guides/other/tutorial-internationalising-your-plugin